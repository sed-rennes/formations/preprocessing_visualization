#  https://pyimagesearch.com/2018/08/20/opencv-text-detection-east-text-detector/
# https://www.kaggle.com/code/voglinio/images-containing-text
# https://betterprogramming.pub/a-practical-guide-to-extract-text-from-images-ocr-in-python-d8c9c30ae74b
from PIL import Image
from pytesseract import pytesseract
import cv2

img = cv2.imread('old_book_crop.png')
# img = img.resize((400,200))
# img.save('sample.png')
text = pytesseract.image_to_string(img)
print(text)

h, w, c = img.shape
boxes = pytesseract.image_to_boxes(img)
img2 = img.copy()
for b in boxes.splitlines():
    b = b.split(' ')
    img2 = cv2.rectangle(img2, (int(b[1]), h - int(b[2])), (int(b[3]), h - int(b[4])), (0, 255, 0), 2)

# cv2.imshow('Raw image', img)
# cv2.imshow('Text extracted', img2)
# cv2.waitKey(0)


