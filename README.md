# Training: Preprocessing and Data visualization

This Git directory is provided as part of the "Preprocessing and data visualization" training on 29 June 2023.

## Notebooks

- [Tabular](tabular/formation_preproc_tabular.ipynb)
- [Text](text/text_imdb.ipynb)
- [Image](/image)
- [Audio](audio/formation_preproc_audio.ipynb)
- [Visualization modules](visualization/visualization.ipynb)

## Slides

- [Slides](Slides_Preprocessing.pdf)

## Contact

For questions, comments, please contact pnria-sed-rennes@inria.fr

## Authors

- BETTON Thomas
- COURTEILLE Hermann
- LEROUX Cyrille
